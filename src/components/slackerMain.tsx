import React, { Component } from 'react';
import {StyleSheet, View } from 'react-native';
import Header from './header';

export function SlackerMain() {
      return(
        <View style={styles.container}>
            <Header title={'main'} />
        </View>
      );
}
  
const styles = StyleSheet.create({
  container: {
    flex: 1
  }

});

export default SlackerMain;