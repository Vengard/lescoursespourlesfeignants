import React from 'react';
import SlackerMain from './src/components/slackerMain';

export default function App() {
  return(
    <SlackerMain />
  );
}
